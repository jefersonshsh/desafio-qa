class Produto
    attr_reader :nome, :preco, :quantidade

    def initialize(nome, preco, quantidade)
        @nome = nome
        @preco = preco
        @quantidade = quantidade
    end
end
