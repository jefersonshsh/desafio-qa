#require 'minitest/autorun'
require "test/unit"
require_relative "../Produto"
require_relative "../CalculaPreco"

class TestCalculaPreco < Test::Unit::TestCase

      def preco(produtos)

          listaRegraPreco = [Produto.new("A", 50, 1),
                             Produto.new("A", 130, 3),
                             Produto.new("B", 30, 1),
                             Produto.new("B", 45, 2),
                             Produto.new("C", 20, 1),
                             Produto.new("D", 15, 1)]

          calculaPreco = CalculaPreco.new listaRegraPreco
          produtos.split(//).each {|item|
                                   calculaPreco.ConsultaPreco(item)}

          calculaPreco.total
      end


      def test_totais
          assert_equal(  0, preco(""))
          assert_equal( 50, preco("A"))
          assert_equal( 80, preco("AB"))
          assert_equal(115, preco("CDBA"))

          assert_equal(100, preco("AA"))
          assert_equal(130, preco("AAA"))
          assert_equal(180, preco("AAAA"))
          assert_equal(230, preco("AAAAA"))
          assert_equal(260, preco("AAAAAA"))

          assert_equal(160, preco("AAAB"))
          assert_equal(175, preco("AAABB"))
          assert_equal(190, preco("AAABBD"))
          assert_equal(190, preco("DABABA"))
      end

      def test_incremental
          listaRegraPreco = [Produto.new("A", 50, 1),
                             Produto.new("A", 130, 3),
                             Produto.new("B", 30, 1),
                             Produto.new("B", 45, 2),
                             Produto.new("C", 20, 1),
                             Produto.new("D", 15, 1)]

          cp = CalculaPreco.new(listaRegraPreco)
          assert_equal( 0, cp.total)
          cp.ConsultaPreco("A"); assert_equal( 50, cp.total)
          cp.ConsultaPreco("B"); assert_equal( 80, cp.total)
          cp.ConsultaPreco("A"); assert_equal( 130, cp.total)
          cp.ConsultaPreco("A"); assert_equal( 160, cp.total)
          cp.ConsultaPreco("B"); assert_equal( 175, cp.total)
    end
end
