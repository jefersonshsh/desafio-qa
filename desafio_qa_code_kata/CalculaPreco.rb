require_relative "Produto"

class CalculaPreco
    def initialize(listaRegraPreco)
        @listaRegraPreco = listaRegraPreco
        @total = 0
        @lstProdAux = []
        @prodComprado = []
        @ocorProd = Hash.new
        @precoUnitario = 0
    end

    def total
        return @total
    end

    def ConsultaPreco(item)
        precoTotal = 0
        qtdCompra = OcorrenciaProduto(item)
        @total = definePreco(item, qtdCompra, @total)
    end

    def OcorrenciaProduto(item)
        @lstProdAux << item
        @ocorProd = Hash[@lstProdAux.group_by { |l| l }.map { |lstProdAux, lstProdAuxs| [lstProdAux, lstProdAuxs.size] }]
        return @ocorProd[item]
    end

    def definePreco(produto, qtdCompra, total)
        precoAux = 0
        precoRegra = 0
        qtdCompraAux = 0
        precoUnitario = 0
        # lista contendo a regra de um determinado produto
        listaAux = @listaRegraPreco.select{ |listaRegraPreco| listaRegraPreco.nome == produto }.sort_by{|l| l.quantidade}.reverse.first

        if listaAux == nil
            qtdCompraAux = 1
        else
            qtdCompraAux = qtdCompra % listaAux.quantidade == 0? listaAux.quantidade : 1
        end

        listaAux = @listaRegraPreco.select{ |listaRegraPreco| listaRegraPreco.nome == produto && listaRegraPreco.quantidade == qtdCompraAux}.first

         precoUnitario = @listaRegraPreco.select{ |listaRegraPreco| listaRegraPreco.nome == produto && listaRegraPreco.quantidade == 1}.first.preco

         if qtdCompraAux == 1
            precoRegra += precoUnitario
         else
            precoRegra = precoRegra - ((listaAux.quantidade - 1) * precoUnitario)
            precoRegra += listaAux.preco
         end

         total += precoRegra

         return total
    end

end
