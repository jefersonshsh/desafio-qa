# language: pt
Funcionalidade: Desabilitar o download automático.
Descrição: Eu como usuário do whatsapp, não quero que app faça download automático de fotos, áudios, videos e documentos quando estiver utilizando rede de dados móveis.

  Contexto:
    # Insira os passos

    @calabash
    Cenário: Desabilitar download automático quando estiver utilizando dados móveis
    Dado que esteja na tela inicial da aplicação
    Quando acesso o menu de itens
    E escolho as configurações
    E seleciono dados e armazenamento
    E seleciono a opção de utilização de rede de dados
    E visualizo a caixa de diálogo com as opções de mídia
    E desmarco as opções de mídia da caixa de diálogo
    E clico em ok
    Então a caixa de diálogo é encerrada
