# language: pt
Funcionalidade: Permitir que somente os contatos vejam a foto do perfil.
Descrição: Eu como usuário do whatsapp, quero que somente os contatos enxerguem a foto do perfil.

  Contexto:
    # Insira os passos

    @calabash
    Cenário: Permitir que somente os contatos visualizem a foto do perfil
    Dado que esteja na tela inicial das aplicação
    Quando acesso o menu de itens
    E escolho as configurações
    E seleciono a opção Conta
    E seleciono a opção Privacidade
    E acesso a opção Foto do perfil
    E visualizo o popup com as opções para exibição
    E marco a opção de meus contatos
    Então o popup é encerrado
