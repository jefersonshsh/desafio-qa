Dado(/^que esteja na tela inicial da aplicação$/) do
  @pages_tela_inicial = page(TelaInicial).await(timeout: 20)
end

Quando(/^acesso o menu de itens$/) do
  @pages_tela_inicial.tocar_menu_itens
end

Quando(/^escolho as configurações$/) do
  @pages_tela_inicial.tocar_menu_itens_configuracoes
end

Quando(/^seleciono dados e armazenamento$/) do
  @pages_tela_confituracao = page(TelaConfiguracao).await(timeout: 20)
  @pages_tela_confituracao.tocar_dados_armazenamento
end

Quando(/^seleciono a opção de utilização de rede de dados$/) do
  @pages_tela_dados_armazenamento = page(TelaDadosArmazenamento).await(timeout: 20)
  @pages_tela_dados_armazenamento.tocar_redes_dados
end

Quando(/^visualizo a caixa de diálogo com as opções de mídia$/) do
  @pages_tela_dados_armazenamento.mostrar_caixa_dialogo
end

Quando(/^desmarco as opções de mídia da caixa de diálogo$/) do
  @pages_tela_dados_armazenamento.desmarcar_caixa_dialogo_foto
  @pages_tela_dados_armazenamento.desmarcar_caixa_dialogo_audio
  @pages_tela_dados_armazenamento.desmarcar_caixa_dialogo_video
  @pages_tela_dados_armazenamento.desmarcar_caixa_dialogo_documento
end

Quando(/^clico em ok/)
  @pages_tela_dados_armazenamento.tocar_link_ok
end

Então(/^Então o popup é encerrado$/) do
  @pages_tela_dados_armazenamento.caixa_dialogo_close
end
