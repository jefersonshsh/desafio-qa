Dado(/^que esteja na tela inicial da aplicação$/) do
  @pages_tela_inicial = page(TelaInicial).await(timeout: 20)
end

Quando(/^acesso o menu de itens$/) do
  @pages_tela_inicial.tocar_menu_itens
end

Quando(/^escolho as configurações$/) do
  @pages_tela_inicial.tocar_menu_itens_configuracoes
end

Quando(/^seleciono a opção Conta$/) do
  @pages_tela_confituracao = page(TelaConfiguracao).await(timeout: 20)
  @pages_tela_confituracao.tocar_opcao_conta
end

Quando(/^seleciono a opção Privacidade$/) do
  @pages_tela_conta = page(TelaConta).await(timeout: 20)
  @pages_tela_conta.tocar_opcao_privacidade
end

Quando(/^acesso a opção Foto do perfil$/) do
  @pages_tela_privacidade = page(TelaPrivacidade).await(timeout: 20)
  @pages_tela_privacidade.tocar_foto_perfil
end

Quando(/^visualizo a caixa de diálogo com as opções de exibição$/) do
  @pages_tela_privacidade.mostrar_caixa_dialogo
end

Quando(/^marco a opção de meus contatos$/) do
  @pages_tela_privacidade.tocar_caixa_dialogo_itens_meus_contatos
end

Então(/^a caixa de diálogo é encerrada$/) do
  @pages_tela_privacidade.caixa_dialogo_close
end
