class DesabilitarDownloadAutomatico < AndroidScreenBase
  # Identificador da tela
  trait(:trait)                 { "* id:'#{layout_name}'" }

  # Declare todos os elementos da tela
  element(:layout_name)                             {  'view_configuracao' }
  element(:menu)                                    {  'menu' }
  element(:itens_configuracoes)                     {  'item_menu_configuracao' }
  element(:dados_armazenamento)                     {  'dados_armazenamento' }
  element(:item_redes_dados)                        {  'redes_dados' }
  element(:caixa_dialogo_download)                  {  'caixa_dialogo_download' }
  element(:item_caixa_dialogo_foto)                 {  'item_caixa_dialogo_foto' }
  element(:item_caixa_dialogo_audio)                {  'item_caixa_dialogo_audio' }
  element(:item_caixa_dialogo_video)                {  'item_caixa_dialogo_video' }
  element(:item_caixa_dialogo_documento)            {  'item_caixa_dialogo_documento' }
  element(:link_ok)                                 {  'link_ok' }

  # Declare todas as acoes da tela
  # action(:touch_button) do
  #   touch("* id:'#{button}'")
  # end

  def tocar_menu_itens
    touch_screen_element menu
  end

  def tocar_menu_itens_configuracoes
    touch_screen_element itens_configuracoes
  end

  def tocar_dados_armazenamento
    touch_screen_element dados_armazenamento
  end

  def tocar_redes_dados
    touch_screen_element item_redes_dados
  end

  def mostrar_caixa_dialogo
    show_screen_element caixa_dialogo_download
  end

  def desmarcar_caixa_dialogo_foto
    uncheck_element item_caixa_dialogo_foto
  end

  def desmarcar_caixa_dialogo_audio
    uncheck_element item_caixa_dialogo_audio
  end

  def desmarcar_caixa_dialogo_video
    uncheck_element item_caixa_dialogo_video
  end

  def desmarcar_caixa_dialogo_documento
    uncheck_element item_caixa_dialogo_documento
  end

  def tocar_link_ok
    touch_screen_element link_ok
  end

  def caixa_dialogo_close
    element_close caixa_dialogo_download
  end
end
