class SetarExibicaoFotoPerfilScreen < AndroidScreenBase
  # Identificador da tela
  trait(:trait)                 { "* id:'#{layout_name}'" }

  # Declare todos os elementos da tela
  element(:layout_name)                             {  'view_configuracao' }
  element(:menu)                                    {  'menu' }
  element(:itens_configuracoes)                     {  'item_menu_configuracao' }
  element(:opcao_conta)                             {  'opcao_conta' }
  element(:opcao_privacidade)                       {  'opcao_privacidade' }
  element(:foto_perfil)                             {  'foto_perfil' }
  element(:caixa_dialogo_perfil)                    {  'caixa_dialogo_perfil' }
  element(:item_caixa_dialogo_meus_contatos)        {  'item_caixa_dialogo_meus_contatos' }


  def tocar_menu_itens
    touch_screen_element menu
  end

  def tocar_menu_itens_configuracoes
    touch_screen_element itens_configuracoes
  end

  def tocar_opcao_conta
    touch_screen_element opcao_conta
  end

  def tocar_opcao_privacidade
    touch_screen_element opcao_privacidade
  end

  def tocar_foto_perfil
    touch_screen_element foto_perfil
  end

  def mostrar_caixa_dialogo
    show_screen_element caixa_dialogo_perfil
  end

  def tocar_caixa_dialogo_itens_meus_contatos
    check_element item_caixa_dialogo_meus_contatos
  end

  def caixa_dialogo_close
    element_close caixa_dialogo_perfil
  end

end
